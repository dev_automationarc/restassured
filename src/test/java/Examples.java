import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map; 

public class Examples {
	
	//@Test
	public void testGet() {
		
		baseURI = "http://localhost:3000/";
		given().get("/users").then().statusCode(200).log().all();
		given().param("name", "Devops").get("/subjects").then().statusCode(200).log().all();
	}
	
	//@Test
	public void testPost() {
		
		baseURI = "http://localhost:3000/";
		JSONObject requestBody = new JSONObject();
		requestBody.put("firstName", "Ena");
		requestBody.put("lastName", "Odolas");
		requestBody.put("subjectId", 1);
		
		
		given().
		header("Content-Type", "application/json").
		contentType(ContentType.JSON).
		accept(ContentType.JSON).
		body(requestBody.toJSONString()).
		when().
		post("/users").
		then().statusCode(201).
		log().all();
		
	}
	
	//@Test
	public void testPatch() {
		
		baseURI = "http://localhost:3000/";
		JSONObject requestBody = new JSONObject();
		requestBody.put("lastName", "Gomes");
		
		given().
		header("Content-Type", "application/json").
		contentType(ContentType.JSON).
		accept(ContentType.JSON).
		body(requestBody.toJSONString()).
		when().
		patch("/users/4").
		then().statusCode(200).
		log().all();
		
	}
	
	//@Test
	public void testPut() {
		
		baseURI = "http://localhost:3000/";
		JSONObject requestBody = new JSONObject();
		requestBody.put("firstName", "Ena");
		requestBody.put("lastName", "Odolas");
		requestBody.put("subjectId", 2);
		
		
		given().
		header("Content-Type", "application/json").
		contentType(ContentType.JSON).
		accept(ContentType.JSON).
		body(requestBody.toJSONString()).
		when().
		put("/users/4").
		then().statusCode(200).
		log().all();
		
	}
	
	@Test
	public void testDelete() {
		
		baseURI = "http://localhost:3000/";
		when().
		delete("/users/4").
		then().statusCode(200).
		log().all();
		
	}

}
