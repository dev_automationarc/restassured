import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map; 

public class TestsPATCH {

	@Test
	public void test_01() {
	
//		With Map
		
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put( "name", "morpheus");
//		map.put("job", "leader");
//		System.out.println(map);
//		JSONObject request = new JSONObject(map);
		

//		Without map		
		JSONObject request = new JSONObject();
//		request.put("name", "Subhendu");
		request.put("job", "Solution Architect");
		
		System.out.println(request);
		System.out.println(request.toJSONString());
		
		given().
		header("Content-Type", "application/json").
		contentType(ContentType.JSON).
		accept(ContentType.JSON).
		body(request.toJSONString()).
		when().
		patch("https://reqres.in/api/users").
		then().statusCode(200).
		log().all();
		
		
		
		
	}
	
}
